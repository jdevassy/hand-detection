% Create a cascade detector object.

% Read a video frame and run the detector.
videoFileReader = vision.VideoFileReader('hand_test2.mov');
videoFrame      = step(videoFileReader);

% Draw the returned bounding box around the detected face.
videoOut = insertObjectAnnotation(videoFrame,'rectangle',bbox,'Face');
figure, imshow(videoOut), title('Detected face');