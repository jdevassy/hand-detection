%% read video 
%xyloObj = VideoReader('hand_train1.webm');
clear all;
close all;

xyloObj = VideoReader('hand_test2.mov');

nFrames = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;

% Preallocate movie structure.
%mov(1:nFrames) = ...
%    struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
%           'colormap', []);

load('hist_shr.mat');
hs = sum(hist_hsv,3);
hs_smooth = imfilter(hs, fspecial('Gaussian',9,3), 'symmetric');
hs_norm = hs_smooth/sum(hs_smooth(:));
binary_mask = [];
threshold = .00005;

% Read one frame at a time.
for k = 50 :10: nFrames-1
 %   mov(k).cdata = read(xyloObj, k);
     cur_frame = read(xyloObj, k);
     hsv_frame = rgb2hsv(cur_frame);
     %v_frame = hsv_frame(:,:,3);         
     prob_img = zeros(size(cur_frame,1), size(cur_frame,2));     
     
     binary_mask = zeros(size(prob_img));    
     
     for i=1:size(cur_frame,1)
        for j=1:size(cur_frame,2)
           %if hsv_frame(i,j,3)>=0
           if hsv_frame(i,j,2)>.3
              %prob_img(i,j) = hue_hist(min(floor(hsv_frame(i,j,1)*256)+1,256));
              prob_img(i,j) = hs_norm(min(floor(hsv_frame(i,j,1)*256)+1,256),...
                  min(floor(hsv_frame(i,j,2)*256)+1,256));
              if prob_img(i,j) >= threshold
                  binary_mask(i,j) = 1;
              end
%                 if prob_img(i,j)<threshold
%                     cur_frame(i,j,:) = [0,0,0];
%                 end
           end
%             else
%                cur_frame(i,j,:) = [0,0,0];
%             end
        end
     end
     
     %morphological          
     binary_mask = imerode(binary_mask, strel('disk',5));
     binary_mask = imdilate(binary_mask, strel('disk',9));
     
     lab_img = bwlabel(binary_mask,8);
     conn_comps = regionprops(lab_img, 'BoundingBox', 'ConvexHull');         
         
     figure(1)
     imshow(binary_mask)
     hold on


    %imagesc(prob_img)
%     imshow(cur_frame)
    %draw rect
    for r = 1:numel(conn_comps)
        rectangle('position', [conn_comps(r).BoundingBox], 'edgecolor', 'r');
        %J = step(shapeInserter, binary_mask, [conn_comps(r).BoundingBox]);
        
        
        Isml = imcrop(cur_frame,[conn_comps(r).BoundingBox]);
     
        I = rgb2gray(Isml);
        edges = edge(I, 'canny', .1);

        %Use ransac to get lines

        [row,col] = find(edges);
        pts = [col,row]';

        [yawn, ptNum] = size(pts);
        X = -ptNum/2:ptNum/2;

        %Ransac parameters
        iterNum = 100;
        thDist = 1;
        thInlrRatio = 0.02;

        sampleNum = 2;
         ptNum = size(pts,2);
        thInlr = round(thInlrRatio*ptNum);
        inlrNum = zeros(1,iterNum);
        theta1 = zeros(1,iterNum);
        rho1 = zeros(1,iterNum);

        for p = 1:iterNum
            % 1. fit using 2 random points
            sampleIdx = randIndex(ptNum,sampleNum);
            ptSample = pts(:,sampleIdx);
            d = ptSample(:,2)-ptSample(:,1);
            d = d/norm(d); % direction vector of the line

            % 2. count the inliers, if more than thInlr, refit; else iterate
            n = [-d(2),d(1)]; % unit normal vector of the line
            dist1 = n*(pts-repmat(ptSample(:,1),1,ptNum));
            inlier1 = find(abs(dist1) < thDist);
            inlrNum(p) = length(inlier1);
            if length(inlier1) < thInlr, continue; end

            ev = princomp(pts(:,inlier1)');
            d1 = ev(:,1);
            theta1(p) = -atan2(d1(2),d1(1)); % save the coefs
            rho1(p) = [-d1(2),d1(1)]*mean(pts(:,inlier1),2);

            %Remove points associate with line
            A1 = pts(1,:);
            A2 = pts(2,:);
            A1(inlier1)=[];
            A2(inlier1)=[];
            pts = [A1;A2];
            ptNum = size(pts,2);
            if ptNum < 4
               break 
            end
            
        end

        figure(2), imshow(edges), hold on
        for i = 1:iterNum
           if inlrNum(i) > thInlr
               k = -tan(theta1(i));
               b = rho1(i)/cos(theta1(i));
               plot(X,k*X+b,'r')
           end   
        end
        
        figure(3), imshow(Isml), hold on
        
        matches = 0;
        theta2 = theta1(theta1 ~= 0);
        [yawn,thSize] = size(theta2);
        for i = 1:thSize
            for j = i+1:thSize
               if abs(theta2(i) - theta2(j)) < 0.1
                   matches = matches + 1;
               end
            end
        end
        matches
        pause
        
        close all
        
     end
    

    
end

%movie(hf, mov, 1, xyloObj.FrameRate);