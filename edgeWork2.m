clc
clear

%Get image of possible hand, or total image with possible hand boundries

%bounding_box = [ 5, 5, 95, 115];
bounding_box = [ 205, 20, 315, 150];
Ibig = imread('wave.jpg');

box = [bounding_box(1), bounding_box(2), bounding_box(3) - bounding_box(1), bounding_box(4) - bounding_box(2)];
Isml = imcrop(Ibig,box);
imshow(Isml)

I = rgb2gray(Isml);
edges = edge(I, 'canny', .5);

%Use ransac to get lines

[row,col] = find(edges);
pts = [col,row]';

[yawn, ptNum] = size(pts);
X = -ptNum/2:ptNum/2;

%Ransac parameters
iterNum = 300;
thDist = 1;
thInlrRatio = 0.05;

sampleNum = 2;
ptNum = size(pts,2);
thInlr = round(thInlrRatio*ptNum);
inlrNum = zeros(1,iterNum);
theta1 = zeros(1,iterNum);
rho1 = zeros(1,iterNum);

for p = 1:iterNum
	% 1. fit using 2 random points
	sampleIdx = randIndex(ptNum,sampleNum);
	ptSample = pts(:,sampleIdx);
	d = ptSample(:,2)-ptSample(:,1);
	d = d/norm(d); % direction vector of the line
	
	% 2. count the inliers, if more than thInlr, refit; else iterate
	n = [-d(2),d(1)]; % unit normal vector of the line
	dist1 = n*(pts-repmat(ptSample(:,1),1,ptNum));
	inlier1 = find(abs(dist1) < thDist);
	inlrNum(p) = length(inlier1);
	if length(inlier1) < thInlr, continue; end
    
	ev = princomp(pts(:,inlier1)');
	d1 = ev(:,1);
	theta1(p) = -atan2(d1(2),d1(1)); % save the coefs
	rho1(p) = [-d1(2),d1(1)]*mean(pts(:,inlier1),2);
    
    %Remove points associate with line
    A1 = pts(1,:);
    A2 = pts(2,:);
    A1(inlier1)=[];
    A2(inlier1)=[];
    pts = [A1;A2];
    ptNum = size(pts,2);
end

figure, imshow(edges), hold on
for i = 1:iterNum
   if inlrNum(i) > thInlr
       k = -tan(theta1(i));
       b = rho1(i)/cos(theta1(i));
       plot(X,k*X+b,'r')
   end
end

rectangle('position',[50 30 10 15],'edgecolor','b')

matches = 0;
%theta1(theta1==0)=[];
theta2 = theta1(theta1 ~= 0);
[yawn,thSize] = size(theta2);
for i = 1:thSize
    for j = i+1:thSize
       if abs(theta2(i) - theta2(j)) < 0.3
           matches = matches + 1;
       end
    end
end
matches    