%% read video 
%xyloObj = VideoReader('hand_train1.webm');
clear all;
close all;

xyloObj = VideoReader('hand_test2.mov');

nFrames = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;

% Preallocate movie structure.
%mov(1:nFrames) = ...
%    struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
%           'colormap', []);

load('hist_shr.mat');
hs = sum(hist_hsv,3);
hs_smooth = imfilter(hs, fspecial('Gaussian',9,3), 'symmetric');
hs_norm = hs_smooth/sum(hs_smooth(:));
binary_mask = [];
threshold = .00005;

% Read one frame at a time.
for k = 50 :50: nFrames-1
 %   mov(k).cdata = read(xyloObj, k);
     cur_frame = read(xyloObj, k);
     hsv_frame = rgb2hsv(cur_frame);
     %v_frame = hsv_frame(:,:,3);         
     prob_img = zeros(size(cur_frame,1), size(cur_frame,2));
     
     
     binary_mask = zeros(size(prob_img));
    
     
     for i=1:size(cur_frame,1)
        for j=1:size(cur_frame,2)            
           %if hsv_frame(i,j,3)>=0
           if hsv_frame(i,j,2)>.3
               %prob_img(i,j) = hue_hist(min(floor(hsv_frame(i,j,1)*256)+1,256));
               prob_img(i,j) = hs_norm(min(floor(hsv_frame(i,j,1)*256)+1,256),...
                  min(floor(hsv_frame(i,j,2)*256)+1,256));
              if prob_img(i,j) >= threshold
                  binary_mask(i,j) = 1;
              end
%                 if prob_img(i,j)<threshold
%                     cur_frame(i,j,:) = [0,0,0];
%                 end
           end
%             else
%                cur_frame(i,j,:) = [0,0,0];
%             end
        end
     end
     
     %morphological          
     binary_mask = imerode(binary_mask, strel('disk',5));
     binary_mask = imdilate(binary_mask, strel('disk',9));
     
     lab_img = bwlabel(binary_mask,8);
     conn_comps = regionprops(lab_img, 'BoundingBox', 'ConvexHull');         
         
     figure(1)
     imshow(binary_mask)
     hold on

     figure(2)
    imagesc(prob_img)
%     imshow(cur_frame)
%     draw rect
    
    figure(3)
    imshow(cur_frame)
    hold on
    
    for r = 1:numel(conn_comps)
        %rectangle('position', [conn_comps(r).BoundingBox], 'edgecolor', 'r');
        
        Isml = imcrop(cur_frame,[conn_comps(r).BoundingBox]);
     
        I = rgb2gray(Isml);
        edges = edge(I, 'canny', .05);

        [H,theta,rho] = hough(edges);

        P = houghpeaks(H,5,'threshold',ceil(0.2*max(H(:))));

        lines = houghlines(edges,theta,rho,P,'FillGap',10,'MinLength',10) ;

%         figure, imshow(edges), hold on 
%         max_len = 0;
%         for k = 1:length(lines)
%            xy = [lines(k).point1; lines(k).point2];
%            plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
% 
%            % Plot beginnings and ends of lines
%            plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
%            plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
% 
%            % Determine the endpoints of the longest line segment
%            len = norm(lines(k).point1 - lines(k).point2);
%            if ( len > max_len)
%               max_len = len;
%               xy_long = xy;
%            end
%         end
% 
%         % highlight the longest line segment
%         plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','red');

        matches = 0;
        Size = length(lines);
        for i = 1:Size
            for j = i+1:Size
               if abs(lines(i).theta - lines(j).theta) < 15
                   matches = matches + 1;
               end
            end
        end
        
        if matches > 50
           %Draw green box
           rectangle('position', [conn_comps(r).BoundingBox], 'edgecolor', 'g');
        else
            %Draw red box
           rectangle('position', [conn_comps(r).BoundingBox], 'edgecolor', 'r');
        end
        
    end
    pause
    close all
    
end

%movie(hf, mov, 1, xyloObj.FrameRate);