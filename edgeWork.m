clc
clear

%Get image of possible hand, or total image with possible hand boundries

Ibig = imread('wave.jpg');
imshow(Ibig);
%imshow(Ibig)
box = [5, 5, 90, 110];
Isml = imcrop(Ibig,box);
figure
imshow(Isml)


% % Directly looking at value channel not as good
% figure('Name','Manual')
% Ihsv = rgb2hsv(Irgb);
% I = Ihsv(:,:,2);
% imshow(I);

%figure('Name','automatic')
I = rgb2gray(Isml);
%imshow(I);

edges = edge(I, 'canny', .5);
[y,x] = size(edges);
% figure
% imshow(edges)

%Use ransac to get lines

pts=[;];
for i = 1:y
    for j = 1:x
        if edges(i,j)
            newpt = [j;i];
            pts = [pts newpt];
        end
    end
end

[yawn, ptNum] = size(pts);
X = -ptNum/2:ptNum/2;

iterNum = 300;
thDist = 1;
thInlrRatio = 0.05;

sampleNum = 2;
ptNum = size(pts,2);
thInlr = round(thInlrRatio*ptNum);
inlrNum = zeros(1,iterNum);
theta1 = zeros(1,iterNum);
rho1 = zeros(1,iterNum);

for p = 1:iterNum
	% 1. fit using 2 random points
	sampleIdx = randIndex(ptNum,sampleNum);
	ptSample = pts(:,sampleIdx);
	d = ptSample(:,2)-ptSample(:,1);
	d = d/norm(d); % direction vector of the line
	
	% 2. count the inliers, if more than thInlr, refit; else iterate
	n = [-d(2),d(1)]; % unit normal vector of the line
	dist1 = n*(pts-repmat(ptSample(:,1),1,ptNum));
	inlier1 = find(abs(dist1) < thDist);
	inlrNum(p) = length(inlier1);
	if length(inlier1) < thInlr, continue; end
    
    %Here we have a successful line calculated, remove points?
    %inlier1 holds points as an index of pts array
    
	ev = princomp(pts(:,inlier1)');
	d1 = ev(:,1);
	theta1(p) = -atan2(d1(2),d1(1)); % save the coefs
	rho1(p) = [-d1(2),d1(1)]*mean(pts(:,inlier1),2);
    
    A1 = pts(1,:);
    A2 = pts(2,:);
    A1(inlier1)=[];
    A2(inlier1)=[];
    pts = [A1;A2];
    ptNum = size(pts,2);
end

figure, imshow(edges), hold on
for i = 1:iterNum
   if inlrNum(i) > thInlr
       k = -tan(theta1(i));
       b = rho1(i)/cos(theta1(i));
       plot(X,k*X+b,'r')
   end
end

% %HOUGH STUFF HAHA
% 
% [H,theta,rho] = hough(edges);
% figure, imshow(imadjust(mat2gray(H)),[],'XData',theta,'YData',rho,...
%         'InitialMagnification','fit');
% xlabel('\theta (degrees)'), ylabel('\rho');
% axis on, axis normal, hold on;
% colormap(hot)
% 
% P = houghpeaks(H,5,'threshold',ceil(0.3*max(H(:))));
% x = theta(P(:,2));
% y = rho(P(:,1));
% plot(x,y,'s','color','black');
% 
% lines = houghlines(edges,theta,rho,P,'FillGap',5,'MinLength',7);
% 
% figure, imshow(Isml), hold on
% max_len = 0;
% for k = 1:length(lines)
%    xy = [lines(k).point1; lines(k).point2];
%    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
% 
%    % Plot beginnings and ends of lines
%    plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
%    plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
% 
%    % Determine the endpoints of the longest line segment
%    len = norm(lines(k).point1 - lines(k).point2);
%    if ( len > max_len)
%       max_len = len;
%       xy_long = xy;
%    end
% end
% 
% % highlight the longest line segment
% plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','red');