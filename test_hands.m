%% read video 
%xyloObj = VideoReader('hand_train1.webm');
clear all;

xyloObj = VideoReader('hand_test2.mov');

nFrames = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;

% Preallocate movie structure.
%mov(1:nFrames) = ...
%    struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
%           'colormap', []);

load('hist_shr.mat');
hs = sum(hist_hsv,3);
hs_smooth = imfilter(hs, fspecial('Gaussian',9,3), 'symmetric');
hs_norm = hs_smooth/sum(hs_smooth(:));
binary_mask = [];
threshold = .00005;

% Read one frame at a time.
for k = 1 :10: nFrames-1
 %   mov(k).cdata = read(xyloObj, k);
     cur_frame = read(xyloObj, k);
     hsv_frame = rgb2hsv(cur_frame);
     %v_frame = hsv_frame(:,:,3);         
     prob_img = zeros(size(cur_frame,1), size(cur_frame,2));
     
     
     binary_mask = zeros(size(prob_img));
    
     
     for i=1:size(cur_frame,1)
        for j=1:size(cur_frame,2)            
           %if hsv_frame(i,j,3)>=0
           if hsv_frame(i,j,2)>.3
               %prob_img(i,j) = hue_hist(min(floor(hsv_frame(i,j,1)*256)+1,256));
               prob_img(i,j) = hs_norm(min(floor(hsv_frame(i,j,1)*256)+1,256),...
                  min(floor(hsv_frame(i,j,2)*256)+1,256));
              if prob_img(i,j) >= threshold
                  binary_mask(i,j) = 1;
              end
%                 if prob_img(i,j)<threshold
%                     cur_frame(i,j,:) = [0,0,0];
%                 end
           end
%             else
%                cur_frame(i,j,:) = [0,0,0];
%             end
        end
     end
     
     %morphological          
     binary_mask = imerode(binary_mask, strel('disk',5));
     binary_mask = imdilate(binary_mask, strel('disk',9));
     
     lab_img = bwlabel(binary_mask,8);
     conn_comps = regionprops(lab_img, 'BoundingBox', 'ConvexHull');         
         
     imshow(binary_mask)


    %imagesc(prob_img)
%     imshow(cur_frame)
    %draw rect
    for r = 1:numel(conn_comps)
        rectangle('position', [conn_comps(r).BoundingBox], 'edgecolor', 'r');
        %J = step(shapeInserter, binary_mask, [conn_comps(r).BoundingBox]);
    end
    

    
end

%movie(hf, mov, 1, xyloObj.FrameRate);