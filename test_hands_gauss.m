%% read video 
%xyloObj = VideoReader('hand_train1.webm');
%clear all;

xyloObj = VideoReader('hand_test2.mov');

nFrames = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;

% Preallocate movie structure.
%mov(1:nFrames) = ...
%    struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
%           'colormap', []);

%load('hs_hist.mat');
%hue_hist = sum(hs,2);

% Read one frame at a time.
for k = 1 : nFrames-1
     cur_frame = read(xyloObj, k);
     hsv_frame = rgb2hsv(cur_frame);
     %v_frame = hsv_frame(:,:,3);         
     prob_img = zeros(size(cur_frame,1), size(cur_frame,2));
     
     for i=1:size(cur_frame,1)
        for j=1:size(cur_frame,2)            
           if hsv_frame(i,j,2)>.3
               %prob_img(i,j) = hue_hist(min(floor(hsv_frame(i,j,1)*256)+1,256));
               prob_img(i,j) = normpdf(h_mean, h_sigma*3, hsv_frame(i,j,1))...
                   * normpdf(s_mean, s_sigma*3, hsv_frame(i,j,2));
                
            end
        end
     end

    imagesc(prob_img)
     
end

%movie(hf, mov, 1, xyloObj.FrameRate);