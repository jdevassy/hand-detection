%% read video 
%xyloObj = VideoReader('hand_train1.webm');
xyloObj = VideoReader('hand_train2.mov');

nFrames = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;

% Preallocate movie structure.
%mov(1:nFrames) = ...
%    struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
%           'colormap', []);

h_mean = 0;
s_mean = 0;
count_pix = 0;

% Read one frame at a time.
for k = 1 :  nFrames-1
 %   mov(k).cdata = read(xyloObj, k);
     cur_frame = im2double(read(xyloObj, k));
     orig_im = cur_frame;
     hsv_frame = rgb2hsv(cur_frame);
     %v_frame = hsv_frame(:,:,3);
    
     bl_ch = cur_frame(:,:,3);
     %norm_im = bl_ch;
     
     for i=1:size(cur_frame,1)
        for j=1:size(cur_frame,2)
            %norm_im = min([cur_frame(i,j,1), cur_frame(i,j,2), cur_frame(i,j,3)]);
            norm_im = hsv_frame(i,j,2);
%             if norm_im<.3
%                 cur_frame(i,j,:) = [0,0,0];                                         

            if norm_im>.3
                h_mean = h_mean + hsv_frame(i,j,1);
                s_mean = s_mean + hsv_frame(i,j,2);
                count_pix = count_pix+1;
            end
%             if bl_ch(i,j)>60
%                 cur_frame(i,j,:) = [0,0,0];                                         
%             if bl_ch(i,j)<=50
%                 hist_hsv(min(floor(hsv_frame(i,j,1)*256)+1,256), min(floor(hsv_frame(i,j,2)*256)+1,256),...
%                     min(floor(hsv_frame(i,j,3)*256)+1,256)) ...
%                     = hist_hsv(min(floor(hsv_frame(i,j,1)*256)+1,256), min(floor(hsv_frame(i,j,2)*256)+1,256),...
%                     min(floor(hsv_frame(i,j,3)*256)+1,256)) +1;
%             end
        end
     end
    %imshow(cur_frame)
    
end

h_mean = h_mean / count_pix;
s_mean = s_mean / count_pix;

var_s =0;
var_h =0;

% Read one frame at a time.
for k = 1 :  nFrames-1
 %   mov(k).cdata = read(xyloObj, k);
     cur_frame = im2double(read(xyloObj, k));
     orig_im = cur_frame;
     hsv_frame = rgb2hsv(cur_frame);
     %v_frame = hsv_frame(:,:,3);
    
     %norm_im = bl_ch;
     
     for i=1:size(cur_frame,1)
        for j=1:size(cur_frame,2)
            %norm_im = min([cur_frame(i,j,1), cur_frame(i,j,2), cur_frame(i,j,3)]);
            norm_im = hsv_frame(i,j,2);
%             if norm_im<.3
%                 cur_frame(i,j,:) = [0,0,0];                                         

            if norm_im>.3
                var_h = var_h +(-h_mean + hsv_frame(i,j,1)).^2;
                var_s = var_s +(-s_mean + hsv_frame(i,j,2)).^2;                
            end
%             if bl_ch(i,j)>60
%                 cur_frame(i,j,:) = [0,0,0];                                         
%             if bl_ch(i,j)<=50
%                 hist_hsv(min(floor(hsv_frame(i,j,1)*256)+1,256), min(floor(hsv_frame(i,j,2)*256)+1,256),...
%                     min(floor(hsv_frame(i,j,3)*256)+1,256)) ...
%                     = hist_hsv(min(floor(hsv_frame(i,j,1)*256)+1,256), min(floor(hsv_frame(i,j,2)*256)+1,256),...
%                     min(floor(hsv_frame(i,j,3)*256)+1,256)) +1;
%             end
        end
     end
    %imshow(cur_frame)
    
end

% Size a figure based on the video's width and height.
hf = figure;
set(hf, 'position', [150 150 vidWidth vidHeight])

% Play back the movie once at the video's frame rate.
%movie(hf, mov, 1, xyloObj.FrameRate);